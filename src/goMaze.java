/* This is a classic robot path finding problem. In another C++ version, I choose to use
 * Dijkstra's algorithm to prevent stack overflowing. In this Java version, I choose to
 * use A* search with iteration instead of recursion due to the same reason. Note that it
 * is possibly caused by Windows platform. The algorithm here assumes no diagonal walking, 
 * so that each step has the same weight, and put weighting on the remaining path length.
 * Both routing and tracing back are called iteratively, with a stack and an ArrayList
 * private members. Potential field is not used, but will be an later on improvement.
 */

import java.io.*;
import java.util.*;

public class goMaze {
	private String instring; // input data
	private int[][] map;
	private Stack<grid> nextList = new Stack<grid>(); // List of Back-tracing grid
	private ArrayList<grid> pathList = new ArrayList<grid>(); // Possible grids for next step
	public void pushNext(grid push){nextList.push(push);}
	public void pushList(grid put){pathList.add(put);}
	public int getListSize(){return pathList.size();}
	
	/*
	 * Parsing input from *.dat to a string
	 */
	public void parData(String fname){
		File file = new File(fname);
		StringBuilder contents = new StringBuilder();
		BufferedReader reader = null;
		try{
			reader = new BufferedReader(new FileReader(file));
			String text = null;
			
			while((text = reader.readLine())!= null){
				contents.append(text).append(System.getProperty("line.separator"));
			}
		}catch (IOException e){
			e.printStackTrace();
		}finally{
			try{
				if(reader != null){
					reader.close();
				}
			}catch (IOException e){
				e.printStackTrace();
			}
		}
		   instring = contents.toString();
	}
	
	/*
	 * Split input string to obstacle grids and the start and end points. Return the
	 * maximum grid for building map
	 */
	public grid toVal(Stack<grid> obs, grid stt, grid end){
		int maxX, maxY;
		maxX = 0; maxY = 0;
		int idx1 = instring.indexOf(":");
		int idx2 = instring.indexOf("\"", idx1);
		String obsString = instring.substring(idx1+1,idx2);
		idx1 = instring.indexOf(":",idx2);
		idx2 = instring.indexOf("\"", idx1);
		String stString = instring.substring(idx1+1,idx2);
		idx1 = instring.indexOf(":",idx2);
		String endString = instring.substring(idx1+1);
		idx1 = obsString.indexOf("[");
		idx2 = obsString.lastIndexOf("]");
		obsString = obsString.substring(idx1+1, idx2);
		
		do{
			int numx, numy;
			idx1 = obsString.indexOf("[");
			idx2 = obsString.indexOf(",", idx1);
			numx = Integer.parseInt(obsString.substring(idx1+1, idx2));
			idx1 = obsString.indexOf("]", idx2);
			numy = Integer.parseInt(obsString.substring(idx2+2, idx1));
			if(numx>maxX)
				maxX = numx;
			if(numy>maxY)
				maxY = numy;
			grid tmp = new grid(numx,numy);
			obs.push(tmp);
			obsString = obsString.substring(idx1+1);
		}while(obsString.length()>0);
		idx1 = stString.indexOf("[");
		idx2 = stString.indexOf(",",idx1);
		stt.setX(Integer.parseInt(stString.substring(idx1+1,idx2)));
		if(Integer.parseInt(stString.substring(idx1+1,idx2))>maxX)
			maxX = Integer.parseInt(stString.substring(idx1+1,idx2));
		idx1 = stString.indexOf("]",idx2);
		stt.setY(Integer.parseInt(stString.substring(idx2+2,idx1)));
		if(Integer.parseInt(stString.substring(idx2+2,idx1))>maxY)
			maxY = Integer.parseInt(stString.substring(idx2+2,idx1));
		idx1 = endString.indexOf("[");
		idx2 = endString.indexOf(",",idx1);
		end.setX(Integer.parseInt(endString.substring(idx1+1,idx2)));
		if(Integer.parseInt(endString.substring(idx1+1,idx2))>maxX)
			maxX = Integer.parseInt(endString.substring(idx1+1,idx2));
		idx1 = endString.indexOf("]",idx2);
		end.setY(Integer.parseInt(endString.substring(idx2+2,idx1)));
		if(Integer.parseInt(endString.substring(idx2+2,idx1))>maxY)
			maxY = Integer.parseInt(endString.substring(idx2+2,idx1));
		
		grid tmp = new grid(maxX, maxY);
		return tmp;
	}
	
	/*
	 * Build map with obstacles being -10, start as 1, end as -5 and to be modified later
	 * by the algorithm, and 0 otherwise.
	 */
	public void buildMap(Stack<grid> obs, grid stt, grid end, grid max){
		map[stt.getX()][stt.getY()] = 1;
		map[end.getX()][end.getY()] = -5;
		while(obs.size()>0){
			grid tmp = obs.pop();
			map[tmp.getX()][tmp.getY()] = -10;
		}	
	}
	
	/*
	 * Write the map to a *.txt for debugging.
	 */
	public void writeTo(String fname, grid max){
		try{
			BufferedWriter fwrite = new BufferedWriter(new FileWriter(fname));
			for(int i = 0; i < max.getX(); i++){
				for(int j = 0; j < max.getY(); j++){
					fwrite.write(String.valueOf(map[i][j]) + "\t");
				}
				fwrite.write("\n");
			}
			fwrite.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/*
	 * Output the final result
	 */
	public void writeList(String fname, boolean make){
		try{
			BufferedWriter fwrite = new BufferedWriter(new FileWriter(fname));
			if(make == false)
				fwrite.write("No solution.");
			else
				while(nextList.size()>0){
					grid idx = nextList.pop();
					fwrite.write(String.valueOf(idx.getX()) + "," + String.valueOf(idx.getY()) + "\r\n");
				}
			fwrite.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/*
	 * Iteratively call this function to get next possible grids, push to pathList, and 
	 * sort the pathList with weighted distance measure.
	 */
	public boolean findPath(grid end){
		grid now = pathList.get(0);
		pathList.remove(0);
		int nowVal = map[now.getX()][now.getY()];
		if(now.eq(end))
			return true;
		else{
			// up
			if(map[now.getX()][now.getY()+1] == 0 || map[now.getX()][now.getY()+1] == -5){
				map[now.getX()][now.getY()+1] = nowVal + 1;
				grid tmp = new grid(now.getX(),now.getY()+1);
				tmp.setD(end, nowVal+1);
				pathList.add(tmp);
			}
			// right
			if(map[now.getX()+1][now.getY()] == 0 || map[now.getX()+1][now.getY()] == -5){
				map[now.getX()+1][now.getY()] = nowVal + 1;
				grid tmp = new grid(now.getX()+1,now.getY());
				tmp.setD(end, nowVal+1);
				pathList.add(tmp);
			}
			// down
			if(map[now.getX()][now.getY()-1] == 0 || map[now.getX()][now.getY()-1] == -5){
				map[now.getX()][now.getY()-1] = nowVal + 1;
				grid tmp = new grid(now.getX(),now.getY()-1);
				tmp.setD(end, nowVal+1);
				pathList.add(tmp);
			}
			// left
			if(map[now.getX()-1][now.getY()] == 0 || map[now.getX()-1][now.getY()] == -5){
				map[now.getX()-1][now.getY()] = nowVal + 1;
				grid tmp = new grid(now.getX()-1,now.getY());
				tmp.setD(end, nowVal+1);
				pathList.add(tmp);
			}
			Collections.sort(pathList, new Comparator<grid>(){
				public int compare(grid t1, grid t2){
					return t1.getD()-t2.getD();
				}});
			return false;
		}
	}
	
	/*
	 * Iteratively call this function to trace from end point back to start for the nearest
	 * route.
	 */
	public boolean traceBack(){
		grid end = nextList.peek();
		int nowVal = map[end.getX()][end.getY()];
		if(nowVal ==1)
			return true;
		if(map[end.getX()][end.getY()+1] == nowVal - 1)
			nextList.push(new grid(end.getX(),end.getY()+1));
		else if(map[end.getX()+1][end.getY()] == nowVal - 1)
			nextList.push(new grid(end.getX()+1,end.getY()));
		else if(map[end.getX()][end.getY()-1] == nowVal - 1)
			nextList.push(new grid(end.getX(),end.getY()-1));
		else if(map[end.getX()-1][end.getY()] == nowVal - 1)
			nextList.push(new grid(end.getX()-1,end.getY()));
		return false;
	}

	
	public static void main(String[] argv){
		goMaze input = new goMaze();
		input.parData("set5.dat");
		
		Stack<grid> obs = new Stack<grid>();
		grid stt = new grid();
		grid end = new grid();
		grid max = input.toVal(obs, stt, end);
		
		input.map = new int[max.getX()+2][max.getY()+2];
		for(int i = 0; i < max.getX(); i++)
			for(int j = 0; j < max.getY(); j++)
				input.map[i][j] = 0;
		input.buildMap(obs, stt, end, max);
		 
		input.pushList(stt);
		boolean tmp = false;
		do{
			tmp = input.findPath(end);
		}while(input.getListSize() > 0 && tmp != true);
		System.out.println(tmp);
		
		boolean idx = false;
		input.pushNext(end);
		if(tmp){
			do{
				idx = input.traceBack();
			}while(idx == false);
		}
		
		input.writeList("set5.txt", tmp);
		//input.writeTo("output.txt", max);
	}
}











