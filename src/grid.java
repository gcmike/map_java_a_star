
public class grid {
	private int gridX;
	private int gridY;
	private int dist;
	public grid(){gridX = 0; gridY = 0; dist = -1;}
	public grid(int inX, int inY){gridX = inX; gridY = inY; dist = -1;}
	public int getX(){return gridX;}
	public int getY(){return gridY;}
	public int getD(){return dist;}
	public void setX(int inX){gridX = inX;}
	public void setY(int inY){gridY = inY;}
	public void setD(grid end, int now){dist = now + 10*Math.abs(gridX-end.getX()) + 10*Math.abs(gridY-end.getY());}
	public boolean eq(grid other){return (gridX == other.getX() && gridY == other.getY());}
}
